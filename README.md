# OBJETIVOS
Desarrollar una aplicacion con control de usuarios para gestion de noticias

# OPCIONES
Para realizar esto puedo utilizar dos kits de inicio:
- Laravel Breeze (este es el que vamos a utilizar)
    - alpine
    - Intertia
- Laravel Jetstream

# REQUISITOS

Necesito el paquete laravel breeze.
Este paquete lo puedo instalar automaticamente cuando creo el proyecto o posteriormente

```php
composer require laravel/breeze --dev
php artisan breeze:install
php artisan migrate
```

# TABLA NOTICIAS

Creo una tabla para gestion de noticias utilizando migraciones y creo el controlador de recursos, el modelo, los factories y los seeders.

```php
php artisan make:model Noticia -mcrfs
```

Ahora realizo las migraciones

# MIGRACIONES

En el archivo de migraciones coloco el codigo para crear la tabla

```php
    public function up(): void
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 200)->nullable();
            $table->text('contenido')->nullable();
            $table->string('foto', 200)->nullable();
            $table->foreignId('user_id')->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }
```
Ejecuto la migracion

```php
php artisan migrate
```

# CONFIGURANDO LA APLICACION

En el archivo env configuramos el idioma, url.


# PROBANDO

Si arrancamos el vite y abrimos enlace probamos la aplicacion

```php
npm run dev
```

Veremos que la pagina que carga es welcome y nos deja registrarnos y loguearnos.

# IDIOMA DE MENSAJES

Colocamos los idiomas de los mensajes en castellano

- Instalamos el paquete

```php
composer require --dev laravel-lang/common
```
- Colocamos el idioma configuramo

```php
php artisan lang:update
```
- Podemos añadir un idioma directamente

```php
php artisan lang:add es
```

- Para que te coloque la carpeta del idioma ingles
```php
php artisan lang:publish
```

# RUTAS

En el archivo de rutas utilizamos los middleware para configurar la autentificacion :
- middleware auth : para verificar el usuario autenticado
- middleware guest: usuario sin autenticar
- middleware verified : usuario autenticado y verificado por email


- en el archivo web.php

```php

/**
 * Estas rutas son accesibles por todo el mundo
 */

Route::get('/', function () {
    return view('welcome');
})->name('home');

/**
 * solo si estas logueado
 */

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        // el middleware auth verifica si el usuario existe
        // el middleware verified es si el usuario esta verificado con el correo 
        return view('dashboard');
    })->name('dashboard');
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    /* 
        esta la he añadido 
        para el controlador de noticias 
    */
    Route::resource('noticias', NoticiaController::class); 
});
```

- En el archivo auth.php

```php
// el middleware guest es para verificar que el usuario esta sin loguear
Route::middleware('guest')->group(function () {
    Route::get('register', [RegisteredUserController::class, 'create'])
        ->name('register');

    Route::post('register', [RegisteredUserController::class, 'store']);

    Route::get('login', [AuthenticatedSessionController::class, 'create'])
        ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store']);

    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
        ->name('password.request');

    Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])
        ->name('password.email');

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
        ->name('password.reset');

    Route::post('reset-password', [NewPasswordController::class, 'store'])
        ->name('password.store');
});

Route::middleware('auth')->group(function () {
    Route::get('verify-email', EmailVerificationPromptController::class)
        ->name('verification.notice');

    Route::get('verify-email/{id}/{hash}', VerifyEmailController::class)
        ->middleware(['signed', 'throttle:6,1'])
        ->name('verification.verify');

    Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
        ->middleware('throttle:6,1')
        ->name('verification.send');

    Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->name('password.confirm');

    Route::post('confirm-password', [ConfirmablePasswordController::class, 'store']);

    Route::put('password', [PasswordController::class, 'update'])->name('password.update');

    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
});
```










