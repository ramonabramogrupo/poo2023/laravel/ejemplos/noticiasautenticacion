<?php

namespace App\Http\Controllers\back;

use App\Http\Controllers\Controller;
use App\Models\Noticia;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\TestStatus\Notice;

class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Auth -> Facades de Laravel
        // Auth::user(); // Devuelve el usuario autenticado
        // Auth::check(); // Devuelve true si hay un usuario autenticado
        // Auth::guest(); // Devuelve true si no hay un usuario autenticado
        // Auth::user()->user_id; // Devuelve el id del usuario
        // Auth::id();
        // Auth::user()->name; // Devuelve el nombre del usuario

        // auth()->check(); // Devuelve true si hay un usuario autenticado
        // auth()->user()->user_id; // Devuelve el id del usuario
        // auth()->user()->name; // Devuelve el nombre del usuario

        // obtengo todas las noticias de todos los usuarios
        //$noticias = Noticia::all();
        // paginacion de contenido
        $noticias = Noticia::paginate(5);
        // renderizo la vista con todas las noticias
        // en la vista de backend 
        // en cada noticia tengo que colocar los botones de CRUD
        return view('back.noticias.index', compact('noticias'));
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('back.noticias.create', [
            'noticia' => new Noticia()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $foto = $request->file('foto');

        $fichero = $foto->store('fotos', 'public');
        $noticia = new Noticia();
        $noticia->fill($request->all());
        $noticia->foto = $fichero;
        $noticia->user_id = Auth::id();
        $noticia->save();

        return redirect()
            ->route('back.noticias.show', $noticia);
    }

    /**
     * Display the specified resource.
     */
    public function show(Noticia $noticia)
    {
        return view('back.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Noticia $noticia)
    {
        return view('back.noticias.edit', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Noticia $noticia)
    {
        //
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $fichero = $foto->store('fotos', 'public');
            $noticia->fill($request->all());
            $noticia->foto = $fichero;
        } else {
            $noticia->fill($request->all());
        }

        $noticia->user_id = Auth::id();
        $noticia->save();

        return redirect()
            ->route('back.noticias.show', $noticia);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Noticia $noticia)
    {
        Storage::disk('public')->delete($noticia->foto);

        $noticia->delete();

        return redirect()
            ->route('back.noticias.index');
    }
    public function listado()
    {
        // leer el id del usuario logueado
        $usuario = Auth::id();

        // obtener las noticias de ese usuario
        $noticias = Noticia::where('user_id', $usuario)->paginate(5);

        return view('back.noticias.index', compact('noticias'));
    }
}
