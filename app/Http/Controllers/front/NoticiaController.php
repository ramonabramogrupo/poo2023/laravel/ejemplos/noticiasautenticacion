<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Noticia;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\TestStatus\Notice;

class NoticiaController extends Controller
{

    /**
     * Debe mostrar todas las noticias
     * en frontEnd
     * No deben aparecer los botones de edicion
     * Solamente un boton para mostrar lo noticia completa
     */
    public function index()
    {
        // obtengo todas las noticias
        $noticias = Noticia::paginate(5);

        // renderizo la vista con todas las noticias
        // en la vista de frontEnd
        return view('front.index', compact('noticias'));
    }


    /**
     * Mostrar toda la noticia en el frontEnd
     */
    public function show(Noticia $noticia)
    {
        return view('front.noticias.show', compact('noticia'));
    }

    public function buscar(Request $request)
    {
        $noticias = Noticia::where('titulo', 'like', '%' . $request->busqueda . '%')
            ->orWhere('contenido', 'like', '%' . $request->busqueda . '%')
            ->paginate(5);

        return view('front.index', compact('noticias'));
    }
}
