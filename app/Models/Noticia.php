<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    use HasFactory;

    protected $table = 'noticias';

    protected $fillable = [
        'titulo',
        'contenido',
        'foto',
        'user_id'
    ];

    /**
     * Relacion con el modelo User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
