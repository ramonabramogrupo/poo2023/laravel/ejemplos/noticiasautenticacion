<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Noticia>
 */
class NoticiaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'titulo' => $this->faker->sentence(3),
            'contenido' => $this->faker->paragraph(10),
            'foto' => $this->faker->imageUrl(640, 480),
            'user_id' => $this->faker->numberBetween(1, 3),
        ];
    }
}
