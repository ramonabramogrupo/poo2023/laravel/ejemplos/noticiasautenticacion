<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'ana',
            'email' => 'test@example.com',
        ]);
        User::factory()->create([
            'name' => 'ramon',
            'email' => 'ramon@alpe.com',
        ]);
        User::factory()->create([
            'name' => 'luisa',
            'email' => 'luisa@alpe.com',
        ]);
        $this->call([
            NoticiaSeeder::class
        ]);
    }
}
