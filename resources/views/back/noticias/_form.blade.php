        <div>
            <label>Titulo</label>
            <input type="text" name="titulo" value="{{ old('titulo', $noticia->titulo) }}">
            @error('titulo')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label>Contenido</label>
            <input type="text" name="contenido" value="{{ old('contenido', $noticia->contenido) }}">
            @error('contenido')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label>Foto</label>
            @if ($noticia->foto)
                <img src="{{ asset('storage/' . $noticia->foto) }}" id="preview">
            @else
                <img id="preview">
            @endif
            <input type="file" name="foto" value="{{ $noticia->foto }}" id="fichero">
            @error('foto')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <button type="submit"
                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Enviar</button>
        </div>
