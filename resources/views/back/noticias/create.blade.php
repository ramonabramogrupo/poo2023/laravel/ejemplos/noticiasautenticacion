<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <form action="{{ route('back.noticias.store') }}" method="POST" enctype="multipart/form-data"
                class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                @csrf
                @include('back.noticias._form')

            </form>
        </div>

    </div>
</x-app-layout>
