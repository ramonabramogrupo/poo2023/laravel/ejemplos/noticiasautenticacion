<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <form action="{{ route('back.noticias.update', $noticia) }}" method="POST" enctype="multipart/form-data"
                class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                @csrf
                @method('PUT')
                @include('back.noticias._form')

            </form>
        </div>

    </div>
</x-app-layout>
