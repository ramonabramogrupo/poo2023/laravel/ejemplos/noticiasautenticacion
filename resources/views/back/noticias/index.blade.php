<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @foreach ($noticias as $noticia)
                <div class="bg-white mt-4 dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                    @if ($noticia->foto)
                        <div class="p-6 text-gray-900 dark:text-gray-100 text-center m-4"
                            style="background-repeat: no-repeat; background-size:contain; background-position: center; width:400px; height:400px; background-image: url('{{ asset('storage/' . $noticia->foto) }}')">

                        </div>
                    @endif
                    <div class="p-6 text-gray-900 dark:text-gray-100">
                        {{ $noticia->titulo }}
                    </div>
                    <div class="p-6 text-gray-900 dark:text-gray-100">
                        {{ $noticia->contenido }}
                    </div>

                    <div class="m-6 text-sm text-left">
                        <div class="text-blue-400 capitalize">Creador: {{ $noticia->user->name }}</div>
                        <div class="text-blue-400">Actualizacion: {{ $noticia->updated_at }}</div>
                    </div>
                    <div class="p-6 text-gray-900 dark:text-gray-100 text-right">
                        <a href="{{ route('back.noticias.show', $noticia) }}" class="text-blue-500"> Mostrar</a>
                        <a href="{{ route('back.noticias.edit', $noticia) }}" class="text-blue-500"> Editar</a>
                        <form action="{{ route('back.noticias.destroy', $noticia) }}" method="POST" id="eliminar"
                            class="inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="text-red-500"> Eliminar</button>
                        </form>
                    </div>

                </div>
            @endforeach
            <div class="mt-4">
                {{ $noticias->links() }}
            </div>
        </div>

    </div>
</x-app-layout>
