<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <h2 class="text-xl font-semibold text-black dark:text-white">
                {{ $noticia->titulo }}
            </h2>
            <div class="mt-4 flex size-60 shrink-0 items-center justify-center rounded-full bg-[#FF2D20]/10 ">
                <img src="{{ asset('storage/' . $noticia->foto) }}" alt="">
            </div>
            <p class="mt-4 text-sm/relaxed">
                {{ $noticia->contenido }}
            </p>
            <div class="mt-6 text-sm text-right">
                Creador: <div class="text-blue-400 capitalize">
                    {{ $noticia->user->name }}
                </div>

                Fecha creacion: <div class="text-blue-400">
                    {{ $noticia->updated_at }}
                </div>
            </div>
            <div class="mt-6 text-left">
                <a href="{{ route('back.noticias.edit', $noticia) }}"
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                    Editar</a>
                <form action="{{ route('back.noticias.destroy', $noticia) }}" method="POST" id="eliminar"
                    class="inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="bg-red-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        Eliminar</button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
