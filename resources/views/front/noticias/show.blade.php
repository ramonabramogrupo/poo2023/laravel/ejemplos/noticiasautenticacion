<x-main-layout>
    <div class="pt-3 sm:pt-5">
        <h2 class="text-xl font-semibold text-black dark:text-white">
            {{ $noticia->titulo }}
        </h2>
        <div class="mt-4 flex size-60 shrink-0 items-center justify-center rounded-full bg-[#FF2D20]/10 ">
            <img src="{{ asset('storage/' . $noticia->foto) }}" alt="">
        </div>
        <p class="mt-4 text-sm/relaxed">
            {{ $noticia->contenido }}
        </p>
        <div class="mt-6 text-sm text-right">
            Creador: <div class="text-blue-400 capitalize">
                {{ $noticia->user->name }}
            </div>

            Fecha creacion: <div class="text-blue-400">
                {{ $noticia->updated_at }}
            </div>
        </div>
    </div>
</x-main-layout>
