<?php

use App\Http\Controllers\back\NoticiaController as BackNoticiaController;
use App\Http\Controllers\front\NoticiaController as FrontNoticiaController;

use App\Http\Controllers\NoticiaController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

// esto lo pueden ver todos los usuarios

// pagina de inicio para el frontend
// Quiero listar todas las noticias con solo algunos campos
// y que cuando pinche en una noticia me muestre la noticia completa
// en el menu quiero que salga Panel Login Registrar

Route::controller(FrontNoticiaController::class)->group(function () {
    // muestra todas las noticias
    Route::get('/', 'index')->name('home');
    // mostrar una sola noticia en el frontEnd
    Route::get('/front/noticias/{noticia}', 'show')->name('front.noticias.show');
    Route::post('/front/noticias/buscar', 'buscar')->name('front.noticias.buscar');
});


// Los usuarios autenticados pueden ver esto
Route::middleware('auth')->group(function () {
    // pagina de inicio para el backend
    // quiero ver informacion del usuario
    // si pulso sobre el icono del menu me lleva al front
    // y un menu Gestion Noticias | Crear Noticia | Noticias del usuario | Dropdown de cerrar sesion (perfil|cerrar sesion)
    Route::get('/back/index', function () {
        return view('back.index');
    })->name('back.index');

    // para la gestion del perfil de usuario
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');

    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');

    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');


    // para mostrar las noticias del usuario logueado
    Route::controller(BackNoticiaController::class)->group(function () {
        Route::get('/back/noticias/listado', 'listado')->name('back.noticias.listado');
    });

    // para la gestion de las noticias
    Route::resource('back/noticias', BackNoticiaController::class)->names('back.noticias');
});



require __DIR__ . '/auth.php';
